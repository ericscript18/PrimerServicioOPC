﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace MiServicio
{
    class ConexionSQL
    {

        //String StrConexion = ConfigurationManager.ConnectionStrings["SQLConexion"].ConnectionString;

        public string error;

        /*
        public void conect()
        {
            SqlConnection sqlConn = new SqlConnection(StrConexion);
            
            sqlConn.Open();
        }
        */

        public DataTable Sp_ObtenerDatos()
        {
            String StrConexion = ConfigurationManager.ConnectionStrings["SQLConexion"].ConnectionString;
            DataTable dsQuery = new DataTable();
            SqlConnection sqlConn = new SqlConnection(StrConexion);

            try
            {
                sqlConn.Open();
                SqlCommand sqlCom = new SqlCommand("SELECT * FROM Device1", sqlConn);
                sqlCom.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(sqlCom);
                da.Fill(dsQuery);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return dsQuery;
        }


        public void Sp_InsertarDatos(int iTag1, int iTag2, DateTime dtCreateAt, ref DateTime dtTagTime)
        {
            String StrConexion = ConfigurationManager.ConnectionStrings["SQLConexion"].ConnectionString;
            SqlTransaction sqlTrans = null;
            SqlConnection sqlConn = new SqlConnection(StrConexion);

            try
            {
                sqlConn.Open();
                sqlTrans = sqlConn.BeginTransaction();
                SqlCommand sqlCom = new SqlCommand("InsertarTags", sqlConn);

                sqlCom.CommandType = CommandType.StoredProcedure;
                sqlCom.Transaction = sqlTrans;

                sqlCom.Parameters.Clear();
                sqlCom.Parameters.AddWithValue("@Tag1", iTag1);
                sqlCom.Parameters.AddWithValue("@Tag2", iTag2);
                sqlCom.Parameters.AddWithValue("@CreateAt", dtCreateAt);
                sqlCom.Parameters.Add("@TagTime", SqlDbType.DateTime).Direction = ParameterDirection.Output;
                sqlCom.ExecuteNonQuery();

                 dtTagTime = (DateTime)sqlCom.Parameters["@TagTime"].Value;

                sqlTrans.Commit();
            }
            catch (Exception e)
            {
                error = e.Message;
                sqlTrans.Rollback();
            }
            finally
            {
                sqlConn.Close();
            }

        }

        public void Eliminar(int iDevice1_Id)
        {
            String StrConexion = ConfigurationManager.ConnectionStrings["SQLConexion"].ConnectionString;
            SqlTransaction sqlTrans = null;
            SqlConnection sqlConn = new SqlConnection(StrConexion);

            String strQuery = ("Delete FROM Device1 Where Device1_Id = " + iDevice1_Id.ToString());

            try
            {
                sqlConn.Open();
                sqlTrans = sqlConn.BeginTransaction();
                SqlCommand sqlCom = new SqlCommand(strQuery, sqlConn);
                sqlCom.CommandType = CommandType.Text;
                sqlCom.Transaction = sqlTrans;



                sqlCom.ExecuteNonQuery();

                sqlTrans.Commit();

            }
            catch (Exception e)
            {
                error = e.Message;
                sqlTrans.Rollback();
            }
            finally
            {
                sqlConn.Close();
            }
        }

        public void Sp_UpdateTags(int iDevice1_Id, int iTag1, int iTag2, DateTime dtCreateAt)
        {
            String StrConexion = ConfigurationManager.ConnectionStrings["SQLConexion"].ConnectionString;
            SqlTransaction sqlTrans = null;
            SqlConnection sqlConn = new SqlConnection(StrConexion);

            try
            {
                sqlConn.Open();
                sqlTrans = sqlConn.BeginTransaction();
                SqlCommand sqlCom = new SqlCommand("UpdateTags", sqlConn);

                sqlCom.CommandType = CommandType.StoredProcedure;
                sqlCom.Transaction = sqlTrans;

                sqlCom.Parameters.Clear();
                sqlCom.Parameters.AddWithValue("@Device1_Id", iDevice1_Id);
                sqlCom.Parameters.AddWithValue("@Tag1", iTag1);
                sqlCom.Parameters.AddWithValue("@Tag2", iTag2);
                sqlCom.Parameters.AddWithValue("@CreateAt", dtCreateAt);

                sqlCom.ExecuteNonQuery();



                sqlTrans.Commit();
            }
            catch (Exception ex)
            {
                error = ex.Message;
                sqlTrans.Rollback();
            }
            finally
            {
                sqlConn.Close();
            }
        }

    }
}