﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Kepware.ClientAce.OpcDaClient;
using Kepware.ClientAce.OpcCmn;
using System.Configuration;



namespace MiServicio
{
    public partial class Service1 : ServiceBase
    {
        DaServerMgt daServerMgt = new DaServerMgt();
        ServerIdentifier[] availableOPCServers;
        String[] AvailableOPCServerList = new String[10];
        

        int activeServerSubscriptionHandle;
        int activeClientSubscriptionHandle;

        String[] OPCItemNameStringArray = new String[2];
        String[] OPCItemValueStringArray = new String[2];
        String[] OPCItemQualityStringArray = new String[2];

        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            OPCItemNameStringArray[0] = "Channel1.Device1.Tag1";
            OPCItemNameStringArray[1] = "Channel1.Device1.Tag2";

            ListOPCServers();
            ConnectToServer();
            SubscribeToOPCDAServerEvents();
            SubscribirItems();
           // InsertarDatos();
            //Desconectar();
        }

        private void ListOPCServers()
        {
            OpcServerEnum opcServerEnum = new OpcServerEnum();
            String nodeName = "localhost";
            bool returnAllServers = false;
            ServerCategory[] serverCatagories = { ServerCategory.OPCDA };

            try
            {
                opcServerEnum.EnumComServer(nodeName, returnAllServers, serverCatagories, out availableOPCServers);
                for (int i=0; i <= 9; i++)
                {
                    AvailableOPCServerList[i] = "";
                }
                    


                if (availableOPCServers.GetLength(0) > 0)
                {
                    int i = 0;
                    foreach (ServerIdentifier opcServer in availableOPCServers)
                    {
                       
                        AvailableOPCServerList[i] = opcServer.ProgID;
                        i++;
                       
                    }
                }
                else
                {
                    //MessageBox.Show("No OPC servers found at node: " + nodeName);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Handled List OPC Servers exception. Reason: " + ex.Message);
            }


        }

        private void Desconectar()
        {
            if (daServerMgt.IsConnected)
                daServerMgt.Disconnect();

        }

        private void ConnectToServer()
        {

            String url = "";
            int selectedServerIndex = 0;
            foreach (String Item in AvailableOPCServerList)
            {
                //Aqui se cambiara el servidor al que se conectaria, en este caso RSLinks
                if (Item.Contains("KEPServerEX"))
                {
                    url = availableOPCServers[selectedServerIndex].Url;
                    break;
                }
                selectedServerIndex++;
            }

            int clientHandle = 1;
            ConnectInfo connectInfo = new ConnectInfo();
            connectInfo.LocalId = "en";
            connectInfo.KeepAliveTime = 60000;
            connectInfo.RetryAfterConnectionError = true;
            connectInfo.RetryInitialConnection = false;
            bool connectFailed = false;


            try
            {
                daServerMgt.Connect(url, clientHandle, ref connectInfo, out connectFailed);
              //  lblMessage.Text = "Conexion:Success";
            }
            catch (Exception ex)
            {
                connectFailed = true;
                //lblMessage.Text = "Conexion:Fail";
            }

        }

        private void SubscribeToOPCDAServerEvents()
        {
            daServerMgt.DataChanged += new DaServerMgt.DataChangedEventHandler(daServerMgt_DataChanged);
        }

        public void daServerMgt_DataChanged(int clientSubscription, bool allQualitiesGood, bool noErrors, ItemValueCallback[] itemValues)
        {


            // We need to forward the callback to the main thread of the application if we access the GUI directly from the callback. 
            //It is recommended to do this even if the application is running in the back ground.
            object[] DCevHndlrArray = new object[4];
            DCevHndlrArray[0] = clientSubscription;
            DCevHndlrArray[1] = allQualitiesGood;
            DCevHndlrArray[2] = noErrors;
            DCevHndlrArray[3] = itemValues;
            DataChanged(clientSubscription, allQualitiesGood, noErrors, itemValues);
            //BeginInvoke(new DaServerMgt.DataChangedEventHandler(DataChanged), DCevHndlrArray);

        }

        public void DataChanged(int clientSubscription, bool allQualitiesGood, bool noErrors, ItemValueCallback[] itemValues)
        {


            try
            {

                if (activeClientSubscriptionHandle == clientSubscription)
                {
                    //itms que cambiaron de valor
                    foreach (ItemValueCallback itemValue in itemValues)
                    {

                        int itemIndex = (int)itemValue.ClientHandle;

                        

                        if (itemValue.Value == null)
                        {
                           // OPCItemTagStringArray[itemIndex].Text = "Unknown";
                           OPCItemValueStringArray[itemIndex] = "Unknown";
                        }
                        else
                        {
                            // OPCItemTagStringArray[itemIndex].Text = itemValue.Value.ToString();
                            OPCItemValueStringArray[itemIndex] = itemValue.Value.ToString();
                            InsertarDatos();
                            
                        }

                        // Update quality control:
                       // OPCItemTagQualityStringArray[itemIndex].Text = itemValue.Quality.Name;
                        OPCItemQualityStringArray[itemIndex] = itemValue.Quality.Name;
                    }
                }
            }
            catch (Exception ex)
            {
                //~~ Handle any exception errors here.
              //  MessageBox.Show("Handled Data Changed exception. Reason: " + ex.Message);
            }

        }

        public void SubscribirItems()
        {
            int itemIndex;

            int clientSubscriptionHandle = 1;
            bool active = true;
            int updateRate = 1000;

            Single deadBand = 0;
            ItemIdentifier[] itemIdentifiers = new ItemIdentifier[2];

            for (itemIndex = 0; itemIndex <= 1; itemIndex++)
            {
                itemIdentifiers[itemIndex] = new ItemIdentifier();
                itemIdentifiers[itemIndex].ItemName = OPCItemNameStringArray[itemIndex];
                itemIdentifiers[itemIndex].ClientHandle = itemIndex;
                itemIdentifiers[itemIndex].DataType = Type.GetType("System.Int16");
            }

            int revisedUpdateRate;

            try
            {
                daServerMgt.Subscribe(clientSubscriptionHandle, active, updateRate, out revisedUpdateRate, deadBand,
                                        ref itemIdentifiers, out activeServerSubscriptionHandle);

                activeClientSubscriptionHandle = clientSubscriptionHandle;

                for (itemIndex = 0; itemIndex <= 1; itemIndex++)
                {
                    if (itemIdentifiers[itemIndex].ResultID.Succeeded == false)
                    {
                        //MessageBox.Show("Failed to add item " + itemIdentifiers[itemIndex].ItemName + " to subscription");
                    }
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Handled Subscribe exception. Reason: " + ex.Message);
            }
        }

        public void InsertarDatos()
        {
            int iTag1 = 0;
            int iTag2 = 0;
            DateTime dtCreateAt;
            DateTime dtTagTime = DateTime.Now;
            ConexionSQL con = new ConexionSQL();

           

            try
            {

                Int32.TryParse(OPCItemValueStringArray[0].ToString(), out iTag1);
                Int32.TryParse(OPCItemValueStringArray[1].ToString(), out iTag2);

               // Int32.TryParse(OPCItemNameStringArray[0].ToString(), out iTag1);
               // Int32.TryParse(OPCItemNameStringArray[1].ToString(), out iTag2);
                
                //Int32.TryParse(itemValue.Value.ToString(), out iTag1);
                //Int32.TryParse(itemValue.Value.ToString(), out iTag2);
                dtCreateAt = DateTime.Now;

                con.Sp_InsertarDatos(iTag1, iTag2, dtCreateAt, ref dtTagTime);

                //  lblMessage.Text = "Insercion Exitosa: " + dtTagTime.ToString();
            }
            catch (Exception ex)
            {
                // lblMessage.Text = ex.Message;
            }
        }

      
        protected override void OnStop()
        {
        }
    }
}
